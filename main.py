dictionary = {
    "this": "Bu, Şu",
    "is": "Olmak",
    "a": "Bir",
    "book": "Kitap",
    "pen": "Kalem"
}

while True:
    sentence = input("Enter a sentence: (or press ENTER to exit) ")

    if sentence == "":
        break

    for signCharacter in ".!?":
        sentence = sentence.replace(signCharacter, "")

    words = set(sentence.lower().split())

    for word in words:
        meaning = dictionary.get(word, "NA")
        print(f"{word.upper()} : {meaning}")
    print()
print("Exit by user...")